import React from 'react';

import Login from './Login';

/**
 * Стартовая страница и подключение авторизации
 */
class WelcomePage extends React.Component {

    /**
     *
     * @type {{password: string, email: string}}
     */
    state = {
        email: '',
        password: ''
    }

    /**
     *
     * @param e
     */
    onEmailChange = (e) => {
        this.setState({
            email: e.target.value
        })
    }

    /**
     *
     * @param e
     */
    onPasswordChahge = (e) => {
        this.setState({
            password: e.target.value
        })
    }

    /**
     *
     * @param e
     */
    onSigninSubmit = (e) => {
        e.preventDefault();
        fetch('http://dnd.v2.auth.local/', {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json;x-www-form-urlencoded',
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(this.state)
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            if (data['status'] === 'success' && data['data']['login'] === 'success') {
                window.open(data['data']['redirectUrl']);
            } else {
                return data
            }
        });
    }

    /**
     *
     * @returns {JSX.Element}
     */
    render() {
        return (
            <Login
                onSigninSubmit={this.onSigninSubmit}
                onEmailChange={this.onEmailChange}
                email={this.state.email}
                password={this.state.password}
                onPasswordChahge={this.onPasswordChahge}
            />
        )
    }
}

export default WelcomePage;